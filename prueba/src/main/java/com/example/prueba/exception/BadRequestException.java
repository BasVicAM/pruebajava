package com.example.prueba.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import lombok.Getter;

@Getter
@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * mensaje.
	 */
	private final String mensaje;

	/**
	 * Empty Constructor.
	 */
	public BadRequestException() {

		super();
		mensaje = "";
	}

	public BadRequestException(String message) {

		super(message);
		this.mensaje = message;
	}

}
