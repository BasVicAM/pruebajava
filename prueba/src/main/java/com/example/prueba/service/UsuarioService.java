package com.example.prueba.service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.prueba.exception.BadRequestException;
import com.example.prueba.model.Phone;
import com.example.prueba.model.Usuario;
import com.example.prueba.modelentity.TelefonoEntity;
import com.example.prueba.modelentity.UsuarioEntity;
import com.example.prueba.repository.TelefonoRepository;
import com.example.prueba.repository.UsuarioRepository;
import com.example.prueba.util.Util;

import lombok.Setter;
import lombok.extern.log4j.Log4j2;

@Setter
@Log4j2
@Service
public class UsuarioService {

	/**
	 * Repositorio para el usuario.
	 */
	@Autowired
	private UsuarioRepository usuarioRepository;

	/**
	 * Repositorio para los telefonos.
	 */
	@Autowired
	private TelefonoRepository telefonoRepository;

	/**
	 * crear al usuario en la base de datos.
	 * 
	 * @param usuario daros del usuario.
	 * @return datos completos del usuario registrado.
	 */
	public UsuarioEntity crearUsuario(Usuario usuario) {

		String email = usuario.getEmail();

		validaciones(email, usuario.getPassword());
		validacionesTelefono(usuario.getPhones());
		Long countUserDB = usuarioRepository.count();
		UUID idUser = UUID.nameUUIDFromBytes(email.concat(countUserDB.toString()).getBytes());
		if (usuarioRepository.existsById(idUser)) {
			log.error("[crearUsuario] Ya esta registrado, no se puede continuar con la creacion del usaurio");
			throw new BadRequestException("Ya esta registrado, no se puede continuar con la creacion del usaurio");
		}
		UsuarioEntity usuarioEntity = new UsuarioEntity();
		usuarioEntity.setId(idUser);
		usuarioEntity.setName(usuario.getName());
		usuarioEntity.setEmail(email);
		usuarioEntity.setPassword(usuario.getPassword());
		usuarioEntity.setCreated(new Date());
		usuarioEntity.setModified(new Date());
		usuarioEntity.setLastLogin(new Date());
		usuarioEntity.setToken(Util.createJWT(idUser.toString(), "SYSTEM", usuario.getName()));
		usuarioEntity.setActive(Boolean.TRUE);
		usuarioRepository.save(usuarioEntity);
		guardarTelefonos(usuario, idUser);
		log.info("[crearUsuario] Usuario Creado exitosamente");
		return usuarioEntity;
	}

	/**
	 * valida que los codigos de los telefonos tengan el largo permitido.
	 * 
	 * @param phones
	 */
	private void validacionesTelefono(List<Phone> phones) {
		if (phones != null && !phones.isEmpty() && phones.stream()
				.anyMatch(phone -> phone.getCityCode().length() > 5 || phone.getCountryCode().length() > 5)) {
			throw new BadRequestException("El codigo de ciudad o pais es mas largo de lo permitido. largo permitido 5");
		}

	}

	/**
	 * persiste en la base de datos los numeros de telefonos.
	 * 
	 * @param usuario
	 * @param idUsuario
	 */
	private void guardarTelefonos(Usuario usuario, UUID idUsuario) {
		log.info("[guardarTelefonos] Guardando telefonos del usuario.");
		for (Phone telefono : usuario.getPhones()) {
			TelefonoEntity tel = new TelefonoEntity();
			tel.setCityode(telefono.getCityCode());
			tel.setCountryCode(telefono.getCountryCode());
			tel.setIdUsuario(idUsuario);
			tel.setNumber(telefono.getNumber());

			telefonoRepository.save(tel);
		}
		log.info("[guardarTelefonos] telefonos guardados exitosamente");
	}

	/**
	 * valida si el correo y pass siguen las reglas de negocio.
	 * 
	 * @param email    correo electronico.
	 * @param password pass de usuario.
	 */
	private void validaciones(String email, String password) {

		if (Util.isEmailInvalido(email)) {
			log.error("[crearUsuario] el correo: ".concat(email)
					.concat(" No sigue el formato correcto ej: aaaaaaa@dominio.cl o es nulo"));
			throw new BadRequestException("El correo No sigue el formato correcto ej: aaaaaaa@dominio.cl o es nulo");
		}

		if (Util.isPassInvalido(password)) {
			log.error("[crearUsuario] La Contraseņa: ".concat(password).concat(" es invalida"));
			throw new BadRequestException("La contraseņa es invalida");
		}

		List<UsuarioEntity> usuarios = usuarioRepository.findAll();

		if (!usuarios.isEmpty() && usuarios.stream().anyMatch(usu -> usu.getEmail().equals(email))) {
			log.error("[crearUsuario] el correo ".concat(email)
					.concat(" Ya esta registrado, no se puede continuar con la creacion del usaurio"));
			throw new BadRequestException("El correo ya registrado");
		}

	}

}
