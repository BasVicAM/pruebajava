package com.example.prueba.model;

import java.util.List;

import lombok.Data;

/**
 * Clase que modela los datos del usuario a crear.
 * 
 * @author vayal
 *
 */
@Data
public class Usuario {

	/**
	 * nombre del usuario.
	 */
	private String name;

	/**
	 * correo del usuario.
	 */
	private String email;

	/**
	 * Contraseņa del usuario.
	 */
	private String password;

	/**
	 * lista de numeros de usuario.
	 */
	private List<Phone> phones;

}
