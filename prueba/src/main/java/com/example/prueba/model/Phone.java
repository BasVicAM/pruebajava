package com.example.prueba.model;

import lombok.Data;

/**
 * Clase que modela los telefonos del usuario.
 * 
 * @author vayal
 *
 */
@Data
public class Phone {

	/**
	 * Numero de telefono
	 */
	private String number;

	/**
	 * Codigo de ciudad.
	 */
	private String cityCode;

	/**
	 * Codigo de pais.
	 */
	private String countryCode;
}
