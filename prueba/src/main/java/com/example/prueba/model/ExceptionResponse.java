package com.example.prueba.model;

import java.io.Serializable;

import lombok.Data;

@Data
public class ExceptionResponse implements Serializable {

	/**
	 * Serial.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Code.
	 */
	private String mensaje;

	public ExceptionResponse(String message) {

		super();
		this.mensaje = message;

	}
}
