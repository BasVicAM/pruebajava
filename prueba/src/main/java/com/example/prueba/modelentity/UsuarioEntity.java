package com.example.prueba.modelentity;

import java.util.Date;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "usuario")
public class UsuarioEntity {

	/**
	 * Id del usuario.
	 */
	@Id
	@Column(name = "id")
	private UUID id;

	/**
	 * nombre del usuario.
	 */
	@Column(name = "name")
	private String name;

	/**
	 * correo del usuario.
	 */
	@Column(name = "email")
	private String email;

	/**
	 * Contraseņa del usuario.
	 */
	@Column(name = "password")
	private String password;

	/**
	 * fecha de creacion del usuario.
	 */
	@Column(name = "createuser")
	private Date created;

	/**
	 * fecha de modificacion del usuario.
	 */
	@Column(name = "modified")
	private Date modified;

	/**
	 * ultimo inicio de sesion del usuario.
	 */
	@Column(name = "last_login")
	private Date lastLogin;

	/**
	 * token del usuario.
	 */
	@Column(name = "token")
	private String token;

	/**
	 * si el usuario esta activo.
	 */
	@Column(name = "isactive")
	private boolean active;

}
