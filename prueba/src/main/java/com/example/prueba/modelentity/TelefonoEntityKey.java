package com.example.prueba.modelentity;

import java.io.Serializable;
import java.util.UUID;

import lombok.Data;

/**
 * Id del telefono.
 * 
 * @author vayal
 *
 */
@Data
public class TelefonoEntityKey implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Id del usuario.
	 */
	private UUID idUsuario;

	/**
	 * Numero de telefono.
	 */
	private String number;

}
