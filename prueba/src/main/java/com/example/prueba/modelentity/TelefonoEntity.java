package com.example.prueba.modelentity;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import lombok.Data;

/**
 * Entidad telefono de la DB.
 * 
 * @author vayal
 *
 */
@Data
@Entity
@IdClass(TelefonoEntityKey.class)
@Table(name = "telefono")
public class TelefonoEntity {

	/**
	 * Id del usuario.
	 */
	@Id
	@Column(name = "id_usuario")
	private UUID idUsuario;

	/**
	 * Numero de telefono.
	 */
	@Id
	@Column(name = "numero")
	private String number;

	/**
	 * Codigo de la ciudad.
	 */
	@Column(name = "citycode")
	private String cityode;

	/**
	 * codigo del pais.
	 */
	@Column(name = "contrycode")
	private String countryCode;

}
