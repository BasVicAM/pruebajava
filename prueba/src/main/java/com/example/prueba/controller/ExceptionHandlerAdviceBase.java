package com.example.prueba.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.prueba.exception.BadRequestException;
import com.example.prueba.model.ExceptionResponse;

import lombok.extern.log4j.Log4j2;

@Log4j2
@RestControllerAdvice
public class ExceptionHandlerAdviceBase extends ResponseEntityExceptionHandler {

	@ExceptionHandler(BadRequestException.class)
	public final ResponseEntity<ExceptionResponse> handleBadRequestException(BadRequestException ex,
			WebRequest request) {

		ExceptionResponse exceptionResponse = null;
		if (ex.getMessage() != null) {
			log.error("Exception handler BadRequestException", ex.getMessage());
			exceptionResponse = new ExceptionResponse(ex.getMessage());
		} else {
			exceptionResponse = new ExceptionResponse("Error en el Json de entrada");
		}
		return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(HttpClientErrorException.class)
	public final ResponseEntity<ExceptionResponse> handleHttpClientErrorException(HttpClientErrorException ex,
			WebRequest request) {

		if (ex.getMessage() != null) {
			log.error("Exception handler HttpClientErrorException", ex);
		}
		ExceptionResponse exceptionResponse = null;
		ResponseEntity<ExceptionResponse> responseEntity = null;
		if (HttpStatus.BAD_REQUEST.equals(ex.getStatusCode())) {
			exceptionResponse = new ExceptionResponse("Bad Request");
			responseEntity = new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
		} else {
			exceptionResponse = new ExceptionResponse("Internal Server Error");
			responseEntity = new ResponseEntity<>(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}

	@ExceptionHandler(HttpServerErrorException.class)
	public final ResponseEntity<ExceptionResponse> handleHttpServerErrorException(HttpServerErrorException ex,
			WebRequest request) {

		if (ex.getMessage() != null) {
			log.error("Exception handler HttpServerErrorException", ex);
		}
		ExceptionResponse exceptionResponse = null;
		ResponseEntity<ExceptionResponse> responseEntity = null;
		if (HttpStatus.SERVICE_UNAVAILABLE.equals(ex.getStatusCode())) {
			exceptionResponse = new ExceptionResponse("Service Unavailable");
			responseEntity = new ResponseEntity<>(exceptionResponse, HttpStatus.SERVICE_UNAVAILABLE);
		} else {
			exceptionResponse = new ExceptionResponse("Internal Server Error");
			responseEntity = new ResponseEntity<>(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}

}
