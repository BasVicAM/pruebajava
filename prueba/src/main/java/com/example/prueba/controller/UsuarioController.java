package com.example.prueba.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.prueba.model.Usuario;
import com.example.prueba.modelentity.UsuarioEntity;
import com.example.prueba.service.UsuarioService;

import lombok.extern.log4j.Log4j2;

@Log4j2
@RequestMapping("/usuario")
@RestController
public class UsuarioController {

	/**
	 * Servici donde se ejecutan las acciones del usuario.
	 */
	@Autowired
	private UsuarioService usuarioService;

	/**
	 * Crea al ussuario en la DB.
	 * 
	 * @param usuario datos de usuario a crear.
	 * @return
	 */
	@PostMapping(value = "/crear")
	public ResponseEntity<UsuarioEntity> crearUsuario(@RequestBody Usuario usuario) {

		log.info("[crearUsuario] Iniciao de creacion de usuario");
		UsuarioEntity usuarioCompleto = usuarioService.crearUsuario(usuario);
		log.info("[crearUsuario] Fin creacion de usaurio.");
		return new ResponseEntity<>(usuarioCompleto, HttpStatus.OK);
	}
}
