package com.example.prueba.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.prueba.modelentity.UsuarioEntity;

/**
 * Repository para realizar las solicitudes del usaurio la la DB.
 * 
 * @author vayal
 *
 */
public interface UsuarioRepository extends JpaRepository<UsuarioEntity, UUID> {

}
