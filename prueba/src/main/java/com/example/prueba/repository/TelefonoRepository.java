package com.example.prueba.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.prueba.modelentity.TelefonoEntity;
import com.example.prueba.modelentity.TelefonoEntityKey;

/**
 * Repository para realizar las solicitudes del telefono la la DB.
 * 
 * @author vayal
 *
 */
public interface TelefonoRepository extends JpaRepository<TelefonoEntity, TelefonoEntityKey> {

}
