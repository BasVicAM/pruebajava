package com.example.prueba.service

import static org.assertj.core.api.InstanceOfAssertFactories.LIST
import static org.assertj.core.api.InstanceOfAssertFactories.list
import static org.hamcrest.CoreMatchers.any
import static org.mockito.ArgumentMatchers.any
import static org.mockito.ArgumentMatchers.anyList

import com.example.prueba.exception.BadRequestException
import com.example.prueba.model.Phone
import com.example.prueba.model.Usuario
import com.example.prueba.modelentity.UsuarioEntity
import com.example.prueba.repository.TelefonoRepository
import com.example.prueba.repository.UsuarioRepository

import groovy.util.logging.Log4j2
import spock.lang.Specification
import spock.lang.Unroll

@Unroll
class UsuarioServiceTest extends Specification {

	def "Bad request correo"() {
		setup:
		def usuario = new Usuario(email: 'email',name: 'prueba',password: 'aS12')
		def service = new UsuarioService()
		when:
		service.crearUsuario(usuario)
		then:
		thrown(BadRequestException)
	}
	
	def "Bad request password"() {
		setup:
		def usuario = new Usuario(email: 'email@d.cl',name: 'prueba',password: 'aS1')
		def service = new UsuarioService()
		when:
		service.crearUsuario(usuario)
		then:
		thrown(BadRequestException)
	}
	
	def "Bad request Correo existe"() {
		setup:
		def usuario = new Usuario(email: 'email@d.cl',name: 'prueba',password: 'aS12')
		def repoUsuario = Spy(UsuarioRepository)
		def usuarioEntity = new UsuarioEntity(email: 'email@d.cl')
		def usuarios = new ArrayList<UsuarioEntity>()
		usuarios.add(usuarioEntity)
		repoUsuario.findAll() >> usuarios
		def service = new UsuarioService(usuarioRepository: repoUsuario)
		when:
		service.crearUsuario(usuario)
		then:
		thrown(BadRequestException)
	}
	
	def "Bad request codigo ciudad mas largo de lo permitido"() {
		setup:
		def telefonos = new ArrayList<Phone>()
		telefonos.add(new Phone(cityCode: '9',countryCode: '123456',number: '123123123'))
		def usuario = new Usuario(email: 'email@d.cl',name: 'prueba',password: 'aS12', phones: telefonos)
		def repoUsuario = Spy(UsuarioRepository)
		def usuarioEntity = new UsuarioEntity(email: 'email@prueba.cl')
		def usuarios = new ArrayList<UsuarioEntity>()
		usuarios.add(usuarioEntity)
		repoUsuario.findAll() >> usuarios
		def service = new UsuarioService(usuarioRepository: repoUsuario)
		when:
		service.crearUsuario(usuario)
		then:
		thrown(BadRequestException)
	}
	
	def "Bad request existe Usuario"() {
		setup:
		def telefonos = new ArrayList<Phone>()
		telefonos.add(new Phone(cityCode: '9',countryCode: '56',number: '123123123'))
		def usuario = new Usuario(email: 'email@d.cl',name: 'prueba',password: 'aS12', phones: telefonos)
		def repoUsuario = Spy(UsuarioRepository)
		def usuarioEntity = new UsuarioEntity(email: 'email@prueba.cl')
		def usuarios = new ArrayList<UsuarioEntity>()
		usuarios.add(usuarioEntity)
		repoUsuario.findAll() >> usuarios
		repoUsuario.count() >> 1
		repoUsuario.existsById(_) >> true
		def service = new UsuarioService(usuarioRepository: repoUsuario)
		when:
		service.crearUsuario(usuario)
		then:
		thrown(BadRequestException)
	}
	
	def "Usuario creado"() {
		setup:
		def telefonos = new ArrayList<Phone>()
		telefonos.add(new Phone(cityCode: '9',countryCode: '56',number: '123123123'))
		def usuario = new Usuario(email: 'email@d.cl',name: 'prueba',password: 'aS12', phones: telefonos)
		def repoUsuario = Spy(UsuarioRepository)
		def repoTelefono = Spy(TelefonoRepository)
		def usuarioEntity = new UsuarioEntity(email: 'email@prueba.cl')
		def usuarios = new ArrayList<UsuarioEntity>()
		usuarios.add(usuarioEntity)
		repoUsuario.findAll() >> usuarios
		repoUsuario.count() >> 1
		repoUsuario.existsById(_) >> false
		repoUsuario.save(_) >> void
		repoTelefono.save(_) >> void
		def service = new UsuarioService(usuarioRepository: repoUsuario,telefonoRepository: repoTelefono)
		when:
		def usuarioCreado = service.crearUsuario(usuario)
		then:
		usuarioCreado != null
	}
	
	

}
